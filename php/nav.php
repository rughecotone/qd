<ul class="primary">
    <li class="collapsible">&#8801;</li>
    <li title="home">
        <a href="index.php">
            <img src="img/svg/002-home.svg" />
            <span>home</span>
        </a>
    </li>
    <li title="library">
        <a href="library.php">
            <img src="img/svg/001-inbox.svg" />
            <span>library</span>
        </a>
    </li>
    <li title="room">
        <a href="#">
            <img src="img/svg/004-down-arrow.svg" />
            <span>room</span>
        </a>
    </li>
    <li title="services">
        <a href="#">
            <img src="img/svg/003-chain.svg" />
            <span>services</span>
        </a>
    </li>
    <li title="about">
        <a href="#">
            <img src="img/svg/006-help.svg" />
            <span>about</span>
        </a>
    </li>
    <li class="end">
        <ul class="secondary">
            <?php
                if ($_SESSION["status"] === 3) {
                    echo '<li title="preferences">
                        <a href="" data-toggle="modal" data-target="#preferences">
                            <img src="img/svg/cog.svg" />
                            <span>preferences</span>
                        </a>
                    </li>
                    <li title="log out">
                        <a href="php/logout.php">
                            <img src="img/svg/005-user.svg" />
                            <span>log out</span>
                        </a>
                    </li>';
                }
                else {
                    echo '<li title="log in / register">
                        <a href="" data-toggle="modal" data-target="#account">
                            <img src="img/svg/005-user.svg" />
                            <span>log in / register</span>
                        </a>
                    </li>';
                }
            ?>
        </ul>
    </li>
</ul>