<?php
    if ($_SESSION["message"]) {
        alert($_SESSION["message"]);
    }
    if ($_SESSION["status"] === -1) {
        session_unset();
        session_destroy();
    }
?>
<div id="background"></div>
<div class="modal fade" id="account" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog cascading-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link <?php if ($_SESSION["status"] !== 2) { echo 'active'; } ?>" data-toggle="tab" href="#login" role="tab">login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#register" role="tab">register</a>
                </li>
                <?php
                    if ($_SESSION["status"] == 2) {
                        echo '<li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#setup" role="tab">setup</a>
                        </li>';
                    }
                ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade <?php if ($_SESSION["status"] !== 2) { echo 'show active'; } ?>" id="login" role="tabpanel">
                    <div class="modal-body">
                        <form class="form" action="php/login.php" method="post">
                            <input type="email" name="email" id="login-email" class="form-control form-control-sm validate" required>
                            <label data-error="wrong" data-success="right" for="login-email">email</label>
                            <input type="password" name="password" id="login-password" class="form-control form-control-sm validate" required>
                            <label data-error="wrong" data-success="right" for="login-password">password</label>
                            <input type="submit" class="btn btn-info w-100" value="log in">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="#">forgot password</a>
                    </div>
                </div>
                <div class="tab-pane fade" id="register" role="tabpanel">
                    <div class="modal-body">
                        <form class="form" action="php/register.php" method="post">
                            <input type="email" name="email" id="register-email" class="form-control form-control-sm validate" required>
                            <label data-error="wrong" data-success="right" for="register-email">email</label>
                            <input type="submit" class="btn btn-info w-100" value="sign up">
                        </form>
                    </div>
                </div>
                <?php
                    if ($_SESSION["status"] == 2) {
                        echo '<div class="tab-pane fade show active" id="setup" role="tabpanel">
                            <div class="modal-body">
                                <form class="form" action="php/set.php" method="post">
                                    <input type="text" name="fname" id="setup-fname" class="form-control form-control-sm validate" required>
                                    <label data-error="wrong" data-success="right" for="setup-fname">first name</label>
                                    <input type="text" name="lname" id="setup-lname" class="form-control form-control-sm validate" required>
                                    <label data-error="wrong" data-success="right" for="setup-lname">last name</label>
                                    <input type="password" name="password" id="setup-fname" class="form-control form-control-sm validate" required>
                                    <label data-error="wrong" data-success="right" for="setup-password">password</label>
                                    <input type="password" name="passwordcheck" id="setup-password" class="form-control form-control-sm validate" required>
                                    <label data-error="wrong" data-success="right" for="setup-passwordcheck">repeat password</label>
                                    <input type="submit" class="btn btn-info w-100" value="set up account">
                                </form>
                            </div>
                        </div>';
                    }
                ?>
            </div>
        </div>
    </div>
</div>
<?php
    if($_SESSION["status"] >= 3) {
        echo '<div class="modal fade" id="preferences" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog cascading-modal" role="document">
                <div class="modal-content">
                    <div class="modal-header">Preferences
                        <button type="button" class="close" data-dismiss="modal" aria-label="close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <a href="php/reset.php" id="reset" class="btn btn-light w-100 d-block mt-3">reset password</a>
                        <form class="form reset" action="php/reset.php" method="post" style="display: none;">
                            <input type="password" name="password" id="reset-password" class="form-control form-control-sm validate" required>
                            <label data-error="wrong" data-success="right" for="reset-password">current password</label>
                            <input type="password" name="newpassword" id="reset-newpassword" class="form-control form-control-sm validate" required>
                            <label data-error="wrong" data-success="right" for="reset-newpassword">new password</label>
                            <input type="password" name="newpasswordcheck" id="reset-passwordcheck" class="form-control form-control-sm validate" required>
                            <label data-error="wrong" data-success="right" for="reset-passwordcheck">repeat new password</label>
                            <input type="submit" class="btn btn-info w-100" value="request password reset">
                        </form>
                        <a href="php/delete.php" id="delete" class="btn btn-danger w-100 d-block mt-3">delete account</a>
                    </div>
                </div>
            </div>
        </div>';
    }
?>