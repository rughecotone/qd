<img src="img/svg/010-fast-forward.svg" class="outer forward" />
<img src="img/svg/014-arrow.svg" class="outer back" />
<img src="img/svg/015-stop-button.svg" class="outer stop" />
<img src="img/svg/012-audio-2.svg" class="outer volume soft" />
<img src="img/svg/005-sound.svg" class="outer volume mute" style="display: none;" />
<img src="img/svg/006-volume.svg" class="outer volume loud" style="display: none;" />
<div class="inner">
    <img src="img/svg/001-play.svg" />
    <img src="img/svg/007-pause.svg" style="display: none;" />
</div>