<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
    session_start();

    require 'mailer/src/Exception.php';
    require 'mailer/src/PHPMailer.php';
    require 'mailer/src/SMTP.php';

    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    use PHPMailer\PHPMailer\SMTP;

    require_once('config.php');

    function sendMail($r, $s, $m) {
        $mail = new PHPMailer(true);
        try {
            $mail->IsSMTP();
            $mail->Host = "smtp.gmail.com";
            $mail->SMTPAuth = true;
            $mail->Username = "cis105223053238@gmail.com";
            $mail->Password = 'g+N3NmtkZWe]m8"M';
            $mail->SMTPSecure = "ssl";
            $mail->Port = 465;
            $mail->SMTPKeepAlive = true;
            $mail->Mailer = "smtp";
            $mail->setFrom("tuf94674@temple.edu", "The Team at Q'd");
            $mail->addReplyTo("tuf94674@temple.edu", "The Team at Q'd");
            $mail->addAddress($r, "Most Treasured User");
            $mail->Subject = $s;
            $mail->Body = $m;
            $mail->send();
        }
        catch (phpmailerException $e) {
            $_SESSION["message"] = 'Email failed to send: ' . $e->errorMessage;
            header('Location: ../index.php');
            exit();
        }
    }

    if (!empty($_POST)) {
        if (isset($_POST["password"])) {
            $oldpassword = $_POST["password"];
        }
        else {
            $_SESSION["message"] = "Please enter a valid password.";
            header('Location: ../index.php');
            exit();
        }
        if (isset($_POST["newpassword"])) {
            $newpassword = $_POST["newpassword"];
        }
        else {
            $_SESSION["message"] = "Please enter a valid password.";
            header('Location: ../index.php');
            exit();
        }
        if (isset($_POST["newpasswordcheck"])) {
            if ($_POST["newpasswordcheck"] !== $_POST["newpassword"]) {
                $_SESSION["message"] = "Please make sure your new passwords match.";
                header('Location: ../index.php');
                exit();
            }
        }
        else {
            $_SESSION["message"] = "Please make sure your new passwords match.";
            header('Location: ../index.php');
            exit();
        }
    }
    else {
        header('Location: ../index.php');
        exit();
    }

    $con = new mysqli(SERVER, USER, PASSWORD, DATABASE);
    if ($con->connect_error) {
        $_SESSION["message"] = 'Database connection failed.';
        header('Location: ../index.php');
        exit();
    }

    $query = mysqli_query($con, "SELECT * FROM final WHERE id='" . $_SESSION["id"] . "';");
    $fetch = mysqli_fetch_assoc($query);

    if ($query->num_rows === 1) {
        $authCode = passGen(12);
        if ($fetch["regstate"] == 3) {
            if ($fetch["password"] === md5($oldpassword)) {
                mysqli_query($con, "UPDATE final SET regstate = 4, authcode = md5('$authCode'), newpass = md5('$newpassword') WHERE id = '" . $_SESSION["id"] . "';");
                sendMail($fetch["email"], 'We received a request to update your password.', 'If this was you, please click the following link:
                    http://cis-linux2.temple.edu/~tuf94674/1056/final/php/confirm.php?email=' . $_SESSION["email"] . '&authcode=' . $authCode);
                $_SESSION["status"] = 4;
                $_SESSION["message"] = 'A confirmation email has been sent to the address you used to register your account.';
                header('Location: ../index.php');
                exit();
            }
            else {
                $_SESSION["message"] = 'Password incorrect.';
                header('Location: ../index.php');
                exit();
            }
        }
        else {
            $_SESSION["message"] = 'You do not have permission to perform this action.';
            header('Location: ../index.php');
            exit();
        }
    }
    elseif ($query->num_rows === 0) {
        $_SESSION["message"] = 'Account not found.';
        header('Location: ../index.php');
        exit();
    }
    else {
        $_SESSION["message"] = 'Query failed. Please contact site administrator.';
        header('Location: ../index.php');
        exit();
    }

    mysqli_free_result($query);
    $con->close();
    header('Location: ../index.php');
    exit();
?>