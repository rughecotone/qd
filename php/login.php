<?php
	session_start();
	require_once("config.php");

	if (!empty($_POST)) {
		if (isset($_POST["email"])) {
			$email = $_POST["email"];
		}
		else {
			$_SESSION["message"] = 'Please enter a valid email address.';
			header('Location: ../index.php');
			exit();
		}
		if (isset($_POST["password"])) {
			$password = md5($_POST["password"]);
		}
		else {
			$_SESSION["message"] = 'Please enter a valid password.';
			header('Location: ../index.php');
			exit();
		}
	}
	else {
		header('Location: ../index.php');
		exit();
	}

	$con = new mysqli(SERVER, USER, PASSWORD, DATABASE);
	if ($con->connect_error) {
		$_SESSION["message"] = 'Database connection failed.';
		header('Location: ../index.php');
		exit();
	}

	$query = mysqli_query($con, "SELECT * FROM final WHERE email='$email';");
	$fetch = mysqli_fetch_assoc($query);

	if ($query->num_rows > 0) {
		if ($fetch["password"] === $password) {
			$_SESSION["status"] = 3;
			$_SESSION["id"] = $fetch["id"];
			$_SESSION["email"] = $fetch["email"];
			$_SESSION["fname"] = $fetch["first"];
			$_SESSION["lname"] = $fetch["last"];
			$_SESSION["message"] = 'You have been successfully logged in. Welcome back, ' . $_SESSION["fname"] . '!';
			header('Location: ../library.php');
			exit();
		}
		else {
			$_SESSION["message"] = 'Password incorrect.';
			header('Location: ../index.php');
			exit();
		}
	}
	elseif ($query->num_rows == 0) {
		$_SESSION["message"] = 'Email not found. Please register first!';
		header('Location: ../index.php');
		exit();
	}
	else {
		$_SESSION["message"] = 'Query failed. Please contact site administrator.';
		header('Location: ../index.php');
		exit();
	}

	mysqli_free_result($query);
	$con->close();
	header('Location: ../index.php');
	exit();
?>