<?php
	session_start();
	require_once('config.php');

	$con = new mysqli(SERVER, USER, PASSWORD, DATABASE);
	if ($con->connect_error) {
		$_SESSION["message"] = 'Database connection failed.';
		header('Location: ../index.php');
		exit();
    }
    
    if ($_SESSION["status"] === 3) {
        mysqli_query($con, "DELETE FROM final WHERE id = " . $_SESSION["id"] . ";");
        $_SESSION["status"] = -1;
        $_SESSION["message"] = 'Account successfully deleted.';
    }
    else {
        $_SESSION["message"] = 'You do not have permission to perform the requested action.';
    }
    
	$con->close();
	header('Location: ../index.php');
	exit();
?>