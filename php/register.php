<?php
	session_start();

	require 'mailer/src/Exception.php';
	require 'mailer/src/PHPMailer.php';
	require 'mailer/src/SMTP.php';

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;
	use PHPMailer\PHPMailer\SMTP;

	require_once('config.php');

	if (!empty($_POST)) {
		if (isset($_POST["email"])) {
			$email = $_POST["email"];
		}
		else {
			$_SESSION["message"] = "Please enter a valid email address.";
			header('Location: ../index.php');
			exit();
		}
	}
	else {
		$_SESSION["message"] = "Please enter a valid email address.";
		header('Location: ../index.php');
		exit();
	}

	function sendMail($r, $s, $m) {
		$mail = new PHPMailer(true);
		try {
			$mail->IsSMTP();
			$mail->Host = "smtp.gmail.com";
			$mail->SMTPAuth = true;
			$mail->Username = "cis105223053238@gmail.com";
			$mail->Password = 'g+N3NmtkZWe]m8"M';
			$mail->SMTPSecure = "ssl";
			$mail->Port = 465;
			$mail->SMTPKeepAlive = true;
			$mail->Mailer = "smtp";
			$mail->setFrom("tuf94674@temple.edu", "The Team at Q'd");
			$mail->addReplyTo("tuf94674@temple.edu", "The Team at Q'd");
			$mail->addAddress($r, "Most Treasured User");
			$mail->Subject = $s;
			$mail->Body = $m;
			$mail->send();
		}
		catch (phpmailerException $e) {
			$_SESSION["message"] = 'Email failed to send: ' . $e->errorMessage;
			header('Location: ../index.php');
			exit();
		}
	}

	$con = new mysqli(SERVER, USER, PASSWORD, DATABASE);
	if ($con->connect_error) {
		$_SESSION["message"] = 'Database connection failed.';
		header('Location: ../index.php');
		exit();
	}

	$query = mysqli_query($con, "SELECT * FROM final WHERE email='$email';");
	$fetch = mysqli_fetch_assoc($query);

	if ($query->num_rows > 0) {
		$_SESSION["message"] = 'Email address already registered.';
		header('Location: ../index.php');
		exit();
	}
	elseif ($query->num_rows < 0) {
		$_SESSION["message"] = 'Query failed. Please contact site administrator.';
		header('Location: ../index.php');
		exit();
	}
	else {
		$authCode = passGen(12);
		mysqli_query($con, "INSERT INTO final (regstate, email, authcode) VALUES (1, '$email', md5('$authCode'));");
		sendMail($email, 'Thanks for registering!', 'Please click here to activate your account:
http://cis-linux2.temple.edu/~tuf94674/1056/final/php/authenticate.php?email=' . $email . '&authcode=' . $authCode);
		$_SESSION["message"] = 'Thank you for registering! A confirmation email with your activation link will be sent to you shortly.';
		$_SESSION["status"] = 1;
	}

	mysqli_free_result($query);
	$con->close();
	header('Location: ../index.php');
	exit();
?>