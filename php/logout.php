<?php
    session_start();
    if ($_SESSION["status"]) {
        $_SESSION["message"] = 'Successfully logged out.';
        $_SESSION["status"] = -1;
    }
    header('Location: ../index.php');
    exit();
?>