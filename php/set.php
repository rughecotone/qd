<?php
	session_start();
	require_once('config.php');

	if (!empty($_POST)) {
		if (isset($_POST["fname"])) {
			$fname = $_POST["fname"];
		}
		else {
			$_SESSION["message"] = "Please enter a valid first name.";
			header('Location: ../index.php');
			exit();
		}
		if (isset($_POST["lname"])) {
			$lname = $_POST["lname"];
		}
		else {
			$_SESSION["message"] = "Please enter a valid last name.";
			header('Location: ../index.php');
			exit();
		}
		if (isset($_POST["password"])) {
			$password = $_POST["password"];
		}
		else {
			$_SESSION["message"] = "Please enter a valid password.";
			header('Location: ../index.php');
			exit();
		}
		if (isset($_POST["passwordcheck"])) {
			if ($_POST["passwordcheck"] !== $_POST["password"]) {
				$_SESSION["message"] = "Please make sure your passwords match.";
				header('Location: ../index.php');
				exit();
			}
		}
		else {
			$_SESSION["message"] = "Please make sure your passwords match.";
			header('Location: ../index.php');
			exit();
		}
	}
	else {
		header('Location: ../index.php');
		exit();
	}

	$con = new mysqli(SERVER, USER, PASSWORD, DATABASE);
	if ($con->connect_error) {
		$_SESSION["message"] = 'Database connection failed.';
		header('Location: ../index.php');
		exit();
	}

	$query = mysqli_query($con, "SELECT * FROM final WHERE id='" . $_SESSION["id"] . "';");
	$fetch = mysqli_fetch_assoc($query);

	if ($query->num_rows === 1) {
		if ($fetch["regstate"] == 2) {
			mysqli_query($con, "UPDATE final SET first = '$fname', last = '$lname', password = md5('$password'), regstate = 3 WHERE id = '" . $_SESSION["id"] . "';");
			$_SESSION["status"] = -1;
			$_SESSION["message"] = 'Password successfully set! Please log in with your credentials.';
			header('Location: ../index.php');
			exit();
		}
		else {
			print $fetch["regstate"];
			header('Location: ../index.php');
			exit();
		}
	}
	elseif ($query->num_rows === 0) {
		$_SESSION["message"] = 'Account not found.';
		header('Location: ../index.php');
		exit();
	}
	else {
		$_SESSION["message"] = 'Query failed. Please contact site administrator.';
		header('Location: ../index.php');
		exit();
	}

	mysqli_free_result($query);
	$con->close();
	header('Location: ../index.php');
	exit();
?>