<?php
	session_start();
    require_once('config.php');

	if (!empty($_GET)) {
		if (isset($_GET["email"])) {
			$email = $_GET["email"];
		}
		else {
			$_SESSION["message"] = "Email address not specified.";
			header('Location: ../index.php');
			exit();
		}
		if (isset($_GET["authcode"])) {
			$authCode = $_GET["authcode"];
		}
		else {
			$_SESSION["message"] = "Authentication code not specified.";
			header('Location: ../index.php');
			exit();
		}
	}
	else {
		header('Location: ../index.php');
		exit();
	}

    $con = new mysqli(SERVER, USER, PASSWORD, DATABASE);
	if ($con->connect_error) {
		$_SESSION["message"] = 'Database connection failed.';
		header('Location: ../index.php');
		exit();
    }
    
    $query = mysqli_query($con, "SELECT * FROM final WHERE email='$email';");
    $fetch = mysqli_fetch_assoc($query);

    if ($query->num_rows == 1) {
		if ($fetch["regstate"] == 4) {
			if ($fetch["authcode"] === md5($authCode)) {
				mysqli_query($con, "UPDATE final SET regstate = 3, password = '" . $fetch["newpass"] . "', newpass = null WHERE email='$email';");
				$_SESSION["status"] = -1;
				$_SESSION["message"] = 'Password successfully reset! Please log in again.';
				header('Location: ../index.php');
				exit();
			}
			else {
				$_SESSION["message"] = 'Authentication code incorrect.';
				header('Location: ../index.php');
				exit();
			}
		}
		else {
			$_SESSION["message"] = 'Password reset request not detected.';
			header('Location: ../index.php');
			exit();
		}
	}
	elseif ($query->num_rows == 0) {
		$_SESSION["message"] = 'Account not found.';
		header('Location: ../index.php');
		exit();
	}
	else {
		$_SESSION["message"] = 'Query failed. Please contact site administrator.';
		header('Location: ../index.php');
		exit();
	}

    mysqli_free_result($query);
	$con->close();
	header('Location: ../index.php');
	exit();
?>