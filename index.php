<!DOCTYPE html>
<html>
<?php
    $title = 'home';
    require_once('php/head.php');
?>
<body>
    <header id="header">
        <?php include_once('php/header.php'); ?>
    </header>
    <main id="main">
        <section id="navigation">
            <?php include_once('php/nav.php'); ?>
        </section>
        <section id="content">
            <div id="carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                    <li data-target="#carousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="img/bannerone.svg" alt="">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="img/bannertwo.svg" alt="">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="img/bannerthree.svg" alt="">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <br>
            </div>
            <p>
            Q'd is a cross-platform mobile web app which aggregates multiple music streaming services in the back end into a single, unified player interface in the front end.  YouTube, Spotify, and Apple Music will be the initial services offered at launch, with potential later expansions to include SoundCloud, Google Play Music, Amazon Prime Music, and Deezer.  The second primary release of the app will add the ability to queue tracks from multiple devices by passing service credentials along with the queue entry, to be accessed on the host device on a one-time basis.  Depending on the terms of use of each individual service's API, the third primary release will transition the player code to the cloud, which multiple devices will be able to stream from in real time to multiply the number of potential simultaneous hardware outputs for the queue.
            </p>
            <br>
            <div class="row m-0">
                <div class="card col-4 bg-dark rounded-0">
                    <img class="card-img-top" src="img/Spotify_Icon_RGB_White.svg" alt="Spotify">
                    <div class="card-body">
                        <p class="card-text">Pick your favorite tunes via Spotify</p>
                    </div>
                </div>
                <div class="card col-4 bg-dark rounded-0">
                    <img class="card-img-top" src="img/Apple_Music_Icon_wht.svg" alt="Apple Music">
                    <div class="card-body">
                        <p class="card-text">Select your favorite tunes via Apple Music</p>
                    </div>
                </div>
                <div class="card col-4 bg-dark rounded-0">
                    <img class="card-img-top" src="img/youtube_dark.svg" alt="YouTube">
                    <div class="card-body">
                    <p class="card-text">Enjoy your favorite tunes via YouTube</p>
                    </div>
                </div>
            </div>

            <div id="player">
                <?php include_once('php/player.php'); ?>
            </div>
        </section>
    </main>
    <footer id="footer">
        <?php include_once('php/footer.php'); ?>
    </footer>
</body>
</html>