$(document).ready(function() {

    $('#navigation .collapsible').on('click', function() {
        $('#main').toggleClass('collapsed');
    });
    
    $('#alert').on('click', function() {
        $(this).fadeOut('fast');
    });

    $('#reset').on('click', function(e) {
        e.preventDefault();
        $('form.reset').toggle('fast');
    });

    $('#delete').on('click', function(e) {
        e.preventDefault();
        if (confirm('Are you sure you want to delete your account? This action is permanent and cannot be undone!')) {
            window.location = $(this).attr('href');
        }
    });

    $('#player .inner').on('click', function() {
        $(this).find('img').toggle();
    });

    $('#player .volume').on('click', function() {
        if ($(this).is('.mute')) {
            $(this).add('.loud').toggle();
        }
        else if ($(this).is('.loud')) {
            $(this).add('.soft').toggle();
        }
        else if ($(this).is('.soft')) {
            $(this).add('.mute').toggle();
        }
    });

    if (parseInt($(document.head).find('meta[name="regstate"]').attr('value')) === 2) {
        $('#navigation a[data-target="#account"]').trigger('click');
    }

});