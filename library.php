<!DOCTYPE html>
<html>
<?php
    $title = 'library';
    require_once('php/head.php');
?>
<body>
    <header id="header">
        <?php include_once('php/header.php'); ?>
    </header>
    <main id="main">
        <section id="navigation">
            <?php include_once('php/nav.php'); ?>
        </section>
        <section id="content">
            <?php
                if (!defined('APP_RAN')) {
                    if ($_SESSION["status"] !== 3) {
                        print "Please create an account first.";
                        die();
                    }
                }
            ?>
            <ul class="nav nav-pills mb-3 text-center d-block" id="pills-tab" role="tablist">
                <li class="nav-item d-inline-block mx-1">
                    <a class="nav-link active" id="pills-albums-tab" data-toggle="pill" href="#pills-albums" role="tab" aria-controls="pills-albums" aria-selected="true">albums</a>
                </li>
                <li class="nav-item d-inline-block mx-1">
                    <a class="nav-link" id="pills-artists-tab" data-toggle="pill" href="#pills-artists" role="tab" aria-controls="pills-artists" aria-selected="false">artists</a>
                </li>
                <li class="nav-item d-inline-block mx-1">
                    <a class="nav-link" id="pills-songs-tab" data-toggle="pill" href="#pills-songs" role="tab" aria-controls="pills-songs" aria-selected="false">songs</a>
                </li>
                </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-albums" role="tabpanel" aria-labelledby="pills-albums-tab">
                    <div class="row">
                        <div class="col col-3 album">
                            <img src="img/art/am.PNG" />
                        </div>
                        <div class="col col-3 album">
                            <img src="img/art/beirut.PNG" />
                        </div>
                        <div class="col col-3 album">
                            <img src="img/art/bg.PNG" />
                        </div>
                        <div class="col col-3 album">
                            <img src="img/art/billie.PNG" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-3 album">
                            <img src="img/art/Capture.PNG" />
                        </div>
                        <div class="col col-3 album">
                            <img src="img/art/floaton.PNG" />
                        </div>
                        <div class="col col-3 album">
                            <img src="img/art/gambino.PNG" />
                        </div>
                        <div class="col col-3 album">
                            <img src="img/art/kendrick.PNG" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-3 album">
                            <img src="img/art/khalid.PNG" />
                        </div>
                        <div class="col col-3 album">
                            <img src="img/art/panic.PNG" />
                        </div>
                        <div class="col col-3 album">
                            <img src="img/art/postm.PNG" />
                        </div>
                        <div class="col col-3 album">
                            <img src="img/art/travis.PNG" />
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-artists" role="tabpanel" aria-labelledby="pills-artists-tab">

                </div>
                <div class="tab-pane fade" id="pills-songs" role="tabpanel" aria-labelledby="pills-songs-tab">
                <div class="song row">
                    <div class="col col-4 song-title">5% TINT</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">8TEEN</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">After the Curtain</div>
                    <div class="col col-4 song-artist">Beirut</div>
                    <div class="col col-4 song-album">Gulag Orkestar</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">American Teen</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Angels</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Another Sad Love Song</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Arabells</div>
                    <div class="col col-4 song-artist">Arctic Monkeys</div>
                    <div class="col col-4 song-album">AM</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">ASTROTHUNDER</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Baby Boy</div>
                    <div class="col col-4 song-artist">Childish Gambino</div>
                    <div class="col col-4 song-album">Awaken My Love!</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Behind The Sea</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Big Lie</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">BLOOD.</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Boogieman</div>
                    <div class="col col-4 song-artist">Childish Gambino</div>
                    <div class="col col-4 song-album">Awaken My Love!</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Brandenberg</div>
                    <div class="col col-4 song-artist">Beirut</div>
                    <div class="col col-4 song-album">Gulag Orkestar</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Bratislava</div>
                    <div class="col col-4 song-artist">Beirut</div>
                    <div class="col col-4 song-album">Gulag Orkestar</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Broken Whiskey Glass</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">BUTTERFLY EFFECT</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">California</div>
                    <div class="col col-4 song-artist">Childish Gambino</div>
                    <div class="col col-4 song-album">Awaken My Love!</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">CAN'T SAY (feat. Dan Oliver)</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">CAROUSEL (feat. Frank Ocean)</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Coaster</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">COFFEE BEAN</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Cold</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Cold Blooded</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Congratulations (feat. Quavo)</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Deja Vu (feat. Justin Bieber)</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">DNA.</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Do I Wanna Know?</div>
                    <div class="col col-4 song-artist">Arctic Monkeys</div>
                    <div class="col col-4 song-album">AM</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Do You Know What I'm Seeing?</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Don't Forget to Remember</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Drowse</div>
                    <div class="col col-4 song-artist">Queen</div>
                    <div class="col col-4 song-album">A Day at the Races</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">DUCKWORTH.</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">ELEMENT.</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Emotion</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Fanny</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">FEAR.</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Feel (feat. Kehlani)</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">FEEL.</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Fireside</div>
                    <div class="col col-4 song-artist">Arctic Monkeys</div>
                    <div class="col col-4 song-album">AM</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">First of May</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Float On</div>
                    <div class="col col-4 song-artist">Modest Mouse</div>
                    <div class="col col-4 song-album">Float On - Single</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Folkin' Around</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">From a Mountain In the Middle of the Cabins</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Go Flex</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">GOD.</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Good Old-Fashioned Lover Boy</div>
                    <div class="col col-4 song-artist">Queen</div>
                    <div class="col col-4 song-album">A Day at the Races</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Have Some Love</div>
                    <div class="col col-4 song-artist">Childish Gambino</div>
                    <div class="col col-4 song-album">Awaken My Love!</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Holiday</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Hopeless</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">HOUSTONFORNICATION</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">How Can You Mend a Broken Heart?</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">How Deep is your Love</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">HUMBLE.</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">I Fall Apart</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">I Have Friends in Holy Spaces</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">I started a Joke</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">I Wanna Be Yours</div>
                    <div class="col col-4 song-artist">Arctic Monkeys</div>
                    <div class="col col-4 song-album">AM</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">I Want It All</div>
                    <div class="col col-4 song-artist">Arctic Monkeys</div>
                    <div class="col col-4 song-album">AM</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">I've gotta Get a Message</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">If I Can't Have You</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Jive Talkin'</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Keep Me</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Knee Socks</div>
                    <div class="col col-4 song-artist">Arctic Monkeys</div>
                    <div class="col col-4 song-album">AM</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Let's Go</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Location</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Lonely Days</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Long Away</div>
                    <div class="col col-4 song-artist">Queen</div>
                    <div class="col col-4 song-album">A Day at the Races</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Love Me</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Love So Right</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">LOVE. (FEAT/ ZACARI.)</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Lovely</div>
                    <div class="col col-4 song-artist">Billie Eilish</div>
                    <div class="col col-4 song-album">Lovely - Single</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">LOYALTY. (FEAT. RIHANNA.)</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">LUST.</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Mad As Rabbits</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Mad Sounds</div>
                    <div class="col col-4 song-artist">Arctic Monkeys</div>
                    <div class="col col-4 song-album">AM</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Massachusetts</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Me and Your Mama</div>
                    <div class="col col-4 song-artist">Childish Gambino</div>
                    <div class="col col-4 song-album">Awaken My Love!</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">More Than A Woman</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Mount Wrociai (Idle Days)</div>
                    <div class="col col-4 song-artist">Beirut</div>
                    <div class="col col-4 song-album">Gulag Orkestar</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">NC-17 (feat. 21 Savage)</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">New York Mining Disaster 1941</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Night Fever</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Nights on Broadway</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Nine In the Afternoon</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Nine In the Afternoon (Radio Mix)</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">NO BYSTANDERS (feat. Juice WRLD & Sheck Wes)</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">No Option</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">No. 1 Party Anthem</div>
                    <div class="col col-4 song-artist">Arctic Monkeys</div>
                    <div class="col col-4 song-album">AM</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">One For the Road</div>
                    <div class="col col-4 song-artist">Arctic Monkeys</div>
                    <div class="col col-4 song-album">AM</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Pas de Cheval</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Patient</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Postcards from Italy</div>
                    <div class="col col-4 song-artist">Beirut</div>
                    <div class="col col-4 song-album">Gulag Orkestar</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Prenzlauerberg</div>
                    <div class="col col-4 song-artist">Beirut</div>
                    <div class="col col-4 song-album">Gulag Orkestar</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">PRIDE.</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">R U Mine?</div>
                    <div class="col col-4 song-artist">Arctic Monkeys</div>
                    <div class="col col-4 song-album">AM</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">R.I.P. SCREW (feat. Swae Lee)</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Redbone</div>
                    <div class="col col-4 song-artist">Childish Gambino</div>
                    <div class="col col-4 song-album">Awaken My Love!</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Rhineland (Heartland)</div>
                    <div class="col col-4 song-artist">Beirut</div>
                    <div class="col col-4 song-album">Gulag Orkestar</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Riot</div>
                    <div class="col col-4 song-artist">Childish Gambino</div>
                    <div class="col col-4 song-album">Awaken My Love!</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Run to Me</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Saved</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Saved By the Bell</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Scenic World</div>
                    <div class="col col-4 song-artist">Beirut</div>
                    <div class="col col-4 song-album">Gulag Orkestar</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">She Had the World</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">She's a Handsome Woman</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Shot Down</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">SICKO MODE (feat. Drake, Swae Lee, Big Hawk)</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">SKELETONS (feat. Pharell Williams, Tame Impala, & The Weekend)</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Snap Out Of It</div>
                    <div class="col col-4 song-artist">Arctic Monkeys</div>
                    <div class="col col-4 song-album">AM</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Somebody to Love</div>
                    <div class="col col-4 song-artist">Queen</div>
                    <div class="col col-4 song-album">A Day at the Races</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Stand Tall</div>
                    <div class="col col-4 song-artist">Childish Gambino</div>
                    <div class="col col-4 song-album">Awaken My Love!</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">STARGAZING</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Stayin' Alive</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">STOP TRYING TO BE GOD (feat. James Blake, Kid Cudi, Stevie Wonder, Philip Bailey</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Teo Torriatte (Let Us Cling Together</div>
                    <div class="col col-4 song-artist">Queen</div>
                    <div class="col col-4 song-album">A Day at the Races</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Terrified</div>
                    <div class="col col-4 song-artist">Childish Gambino</div>
                    <div class="col col-4 song-album">Awaken My Love!</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">That Green Gentleman (Things Have Changed)</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">The Bunker</div>
                    <div class="col col-4 song-artist">Beirut</div>
                    <div class="col col-4 song-album">Gulag Orkestar</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">The Canals of Our City</div>
                    <div class="col col-4 song-artist">Beirut</div>
                    <div class="col col-4 song-album">Gulag Orkestar</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">The Gulag Orkestar</div>
                    <div class="col col-4 song-artist">Beirut</div>
                    <div class="col col-4 song-album">Gulag Orkestar</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">The Millionaire Waltz</div>
                    <div class="col col-4 song-artist">Queen</div>
                    <div class="col col-4 song-album">A Day at the Races</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">The Night Me and Your Mama Met</div>
                    <div class="col col-4 song-artist">Childish Gambino</div>
                    <div class="col col-4 song-album">Awaken My Love!</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">The Piano Knows Something I Don't Know</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Therapy</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Tie Your Mother Down</div>
                    <div class="col col-4 song-artist">Queen</div>
                    <div class="col col-4 song-album">A Day at the Races</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">To Love Somebody</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Too Much Heaven</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Too Young</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Up There</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">WAKEUP (feat. The Weekend)</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">We're So Starving</div>
                    <div class="col col-4 song-artist">Panic! At the Disco</div>
                    <div class="col col-4 song-album">Pretty. Odd.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">White Iverson</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">White Man</div>
                    <div class="col col-4 song-artist">Queen</div>
                    <div class="col col-4 song-album">A Day at the Races</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">WHO? WHAT! (feat. Quave & Takeoff)</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Why'd You Only Call Me When You're High?</div>
                    <div class="col col-4 song-artist">Arctic Monkeys</div>
                    <div class="col col-4 song-album">AM</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Winter</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Words</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">World</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">XXX. (FEAT. U2.)</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">YAH.</div>
                    <div class="col col-4 song-artist">Kendrick Lamar</div>
                    <div class="col col-4 song-album">DAMN.</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">YOSEMITE (feat. Gunna & NAV)</div>
                    <div class="col col-4 song-artist">Travis Scott</div>
                    <div class="col col-4 song-album">ASTROWORLD</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">You and I</div>
                    <div class="col col-4 song-artist">Queen</div>
                    <div class="col col-4 song-album">A Day at the Races</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">You Should Be Dancing</div>
                    <div class="col col-4 song-artist">Bee Gees</div>
                    <div class="col col-4 song-album">Their Greatest Hits The Record</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">You Take My Breath Away</div>
                    <div class="col col-4 song-artist">Queen</div>
                    <div class="col col-4 song-album">A Day at the Races</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Young Dumb & Broke</div>
                    <div class="col col-4 song-artist">Khalid</div>
                    <div class="col col-4 song-album">American Teen</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Yours Truly, Austin Post</div>
                    <div class="col col-4 song-artist">Post Malone</div>
                    <div class="col col-4 song-album">Stoney</div>
                </div>
                <div class="song row">
                    <div class="col col-4 song-title">Zombies</div>
                    <div class="col col-4 song-artist">Childish Gambino</div>
                    <div class="col col-4 song-album">Awaken My Love!</div>
                </div>
                </div>
            </div>
            <div id="player">
                <?php include_once('php/player.php'); ?>
            </div>
        </section>
    </main>
    <footer id="footer">
        <?php include_once('php/footer.php'); ?>
    </footer>
</body>
</html>